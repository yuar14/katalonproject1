<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_CURA Healthcare_fa fa-bars</name>
   <tag></tag>
   <elementGuidId>fc56527d-6ade-47de-8341-2995e755f4e1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>i.fa.fa-bars</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='menu-toggle']/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>4ba5a072-99a4-404a-8d4c-fbef5fcab063</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-bars</value>
      <webElementGuid>0afdbbdf-40e1-477a-8746-9df6f1ee9f5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;menu-toggle&quot;)/i[@class=&quot;fa fa-bars&quot;]</value>
      <webElementGuid>5b5e7cd7-a294-4b4e-b32a-bf45a3c0ebac</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='menu-toggle']/i</value>
      <webElementGuid>39e4713a-e93f-4a77-970d-7c0d5f3eb493</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//i</value>
      <webElementGuid>1d31f0d7-c344-45bc-a174-50db4d5d8e1e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
