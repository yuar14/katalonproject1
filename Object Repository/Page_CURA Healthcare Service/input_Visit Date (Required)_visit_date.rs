<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Visit Date (Required)_visit_date</name>
   <tag></tag>
   <elementGuidId>30ad9d83-e1c0-4359-bced-bdd7ef9a8953</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#txt_visit_date</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='txt_visit_date']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>752f5628-0c4c-4063-96b4-d5be37853528</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>1adf4790-1ae8-4b74-a4b8-6d045fdb7be0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>c63f2ad6-fd87-4f9e-b717-6662a7bcbac0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>txt_visit_date</value>
      <webElementGuid>7a625681-1b45-40cc-8a3d-925eda65e29e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>visit_date</value>
      <webElementGuid>4f8524dd-514e-45b7-8e3d-a9ffb055cd5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>dd/mm/yyyy</value>
      <webElementGuid>20565605-acc6-46d2-98cf-0b95219b8c65</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>8f7aac57-2925-45c5-b058-c7c3c01450bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;txt_visit_date&quot;)</value>
      <webElementGuid>59d8194b-2def-4864-a567-26b5ffc6a1f2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='txt_visit_date']</value>
      <webElementGuid>9d7e6d07-2b7d-4bf1-ab2c-b8026b07fd6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[4]/div/div/input</value>
      <webElementGuid>6e75cb6c-59d3-4fe7-98e3-4f8b9324f2fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input</value>
      <webElementGuid>7f61a4d1-58e7-401e-bf11-76c1a97d7cb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @id = 'txt_visit_date' and @name = 'visit_date' and @placeholder = 'dd/mm/yyyy']</value>
      <webElementGuid>69ff9584-afef-4a9d-a9fa-9c963593674d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
